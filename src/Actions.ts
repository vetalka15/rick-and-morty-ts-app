import { IState, IAction, IEpisode } from "./interfaces";

export const fetchDataAction = async (dispatch: any) => {
  const URL = "https://api.tvmaze.com/singlesearch/shows?q=rick-&-morty&embed=episodes";
  const data = await fetch(URL);
  const dataJSON = await data.json();
  return dispatch({ 
    type: "FETCH_DATA", 
    payload: dataJSON._embedded.episodes
  })
}

export const toggleFavAction = (state: IState, dispatch: any, episode: IEpisode | any):IAction => {
  const isFav = state.favourites.includes(episode);

  let dispatchObj = {
    type: 'ADD_FAV',
    payload: episode
  }

  if(isFav) {
    const newFavourites = state.favourites.filter((ep: IEpisode) => ep.id !== episode.id);

    dispatchObj = {
      type: 'REMOVE_FAV',
      payload: newFavourites
    }
  }
  return dispatch(dispatchObj)
}