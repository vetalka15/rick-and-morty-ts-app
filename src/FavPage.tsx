import React, {useContext} from 'react'
import { IEpisodeProps } from "./interfaces"
import { fetchDataAction, toggleFavAction } from "./Actions"
import {Store} from './Store'

const EpisodesList = React.lazy<any>(() => import('./EpisodesList'))

export default function FavPage(): JSX.Element {
  const {state, dispatch} = useContext(Store);

  React.useEffect(() => {
    state.episodes.length === 0 && fetchDataAction(dispatch);
  })

  const props: IEpisodeProps = {
    toggleFavAction,
    store: { state, dispatch },
    favourites: state.favourites,
    episodes: state.favourites
  }
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <section className="episode-layout">
        <EpisodesList { ...props }/>
      </section>
    </React.Suspense>
  )
}
