import React, {useContext} from 'react'
import {Store} from './Store'
import { IEpisodeProps } from "./interfaces"
import { fetchDataAction, toggleFavAction } from "./Actions"

const EpisodesList = React.lazy<any>(() => import('./EpisodesList'))

export default function HomePage() {
  const {state, dispatch} = useContext(Store);

  React.useEffect(() => {
    state.episodes.length === 0 && fetchDataAction(dispatch);
  })

  const props: IEpisodeProps = {
    toggleFavAction,
    store: { state, dispatch },
    favourites: state.favourites,
    episodes: state.episodes
  }
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <section className="episode-layout">
        <EpisodesList { ...props }/>
      </section>
    </React.Suspense>
  )
}
