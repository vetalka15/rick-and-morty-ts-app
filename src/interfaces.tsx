/**
|--------------------------------------------------
| All the interfaces!
|--------------------------------------------------
*/
export type Dispatch = React.Dispatch<IAction>

export interface IState {
  episodes: Array<IEpisode>,
  favourites: Array<any>
}

export interface IAction {
  type: string,
  payload: Array<IEpisode>
}

export interface IEpisode {
  airdate: string
  airstamp: string
  airtime: string
  id: number
  image: {
    medium: string, 
    original: string
  }
  name: string
  number: number
  runtime: number
  season: number
  summary:  string
  url: string
}

export interface IEpisodeProps {
  toggleFavAction: (state: IState, episode: IEpisode, dispatch: Dispatch) => IAction,
  store: { state: IState, dispatch: Dispatch }
  favourites: Array<IEpisode>,
  episodes: Array<IEpisode>
}