import React, {useContext} from 'react';
import {Store} from './Store'
import {Link} from "@reach/router"

export default function App(props:any):JSX.Element {
  const {state} = useContext(Store);
  return (
    <>
      <header>
        <h1>Roick and Morty</h1>
        <div>
          <Link to="/" >Home</Link>
          <Link to="/faves" >Favourite(s): {state.favourites.length}</Link>
        </div>
      </header>
      {props.children}
    </>
  )
}

