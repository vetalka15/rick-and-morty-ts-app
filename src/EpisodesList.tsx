import React from 'react'
import { IEpisode } from "./interfaces"

export default function EpisodesList(props:any):Array<JSX.Element> {
  const { episodes, favourites, toggleFavAction, store } = props;
  const {state, dispatch} = store;
  return (
    episodes.map((episode: IEpisode) => (
      <div key={episode.id} className="episode-box">
        { episode.image && <img src={episode.image.medium} alt="img"/> }
        <div>{episode.name}</div>
        <section style={{ display: 'flex', justifyContent: 'space-between' }} >
          <div>Seasion: {episode.season} Number: {episode.number}</div>
          <button type='button' onClick={() => toggleFavAction(state, dispatch, episode)} >
            {favourites.includes(episode) ? "Unfav" : "Fav"}
          </button>
        </section>
      </div>
    ))
  )
}
